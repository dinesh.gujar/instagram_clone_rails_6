class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_post, only: [:show]

  def index
    @posts = Post.all.includes(:photos, :user, :likes).order(created_at: :desc)
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(content: params[:post][:content])
=begin
    if @post.save
      redirect_to posts_path, notice: "Succesfully uploaded"
    else
      render "new"
    end
=end
    if @post.save
      puts "saved "

      @post.photos.create(image: params[:post][:image])
      #puts params[:post][:images]
      #params[:images].each do |img|
      #  @post.photos.create(image: img)
      #  puts "This is", params[:images]
      flash[:notice] = "saved"
      redirect_to posts_path
      #end
      #end
    else
      flash[:alert] = "Something went wrong "
      redirect_to posts_path
    end
  end

  def destroy
    @post = Post.find(params[:id])
    if @post.destroy
      flash[:notice] = "successfully deleted"
    else
      flash[:notice] = "something went wrong"
    end
    redirect_to posts_path
  end

  def show
    @posts = current_user.posts.includes(:photos, :user, :likes).order(created_at: :desc)
    #@comments = Comment.new
    #@likes = @post.likes.includes(:user)
    #@is_liked = @post.is_liked(current_user)
  end

  private

  def find_post
  end

  def post_params
    params.require(:post).permit(:content, :image)
  end
end
