class FeaturedController < ApplicationController
  def show
    @featured = Post.where(featured: true)
  end
end
