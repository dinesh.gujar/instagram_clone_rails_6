class UsersController < ApplicationController
  def index
    term = params[:term]
    @users = User.where("name like '%#{term}%'")
    respond_to :js
  end

  def show
    @user = User.find(params[:id])
  end
end
