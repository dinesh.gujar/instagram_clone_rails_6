module ApplicationHelper
  def avatar_url(user)
    if user.image
      return user.image
    else
      return "https://brighterwriting.com/wp-content/uploads/icon-user-default-420x420.png"
    end
  end
end
