Rails.application.routes.draw do
  root "posts#index"
  devise_for :users,
             path: "",
             path_names: { sign_in: "login", sign_out: "logout", edit: "profile", sign_up: "registration" },
             controllers: { omniauth_callbacks: "omniauth_callbacks" }
  #get "users/:id", to: "users#show"
  resources :users, only: [:index, :show, :sear]
  #get "users/:term", to: "users#index"
  resources :posts, only: [:index, :create, :show, :destroy] do
    resources :likes, only: [:create, :destroy, :index, :show, :new, :edit, :update], shallow: true
    resources :comments, only: [:index, :create, :destroy]
  end
  #get "posts/show", to: "posts#show"
  #root "posts#show"
  get "/featured", to: "featured#show"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
