class Defaultfeatured < ActiveRecord::Migration[6.0]
  def change
    change_column_default :posts, :Featured, false
  end
end
