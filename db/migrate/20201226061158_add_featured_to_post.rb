class AddFeaturedToPost < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :Featured, :boolean
  end
end
